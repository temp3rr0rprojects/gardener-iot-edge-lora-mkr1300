# Gardener IoT Edge LoRa MKR1300 #

Gardener IoT Edge LoRa MKR1300: LoRa one-way end-point for water valve and water flow sensing on an Arduino MKR 1300.

![alternativetext](https://store-cdn.arduino.cc/uni/catalog/product/cache/1/image/1040x660/604a3538c15e081937dbfbd20aa60aad/a/b/abx00017_featured.jpg)

## SDKs & Libraries
- LoRa.h
- SPI.h

## Hardware
Arduino MKR WAN 1300 (LoRa connectivity) (https://store.arduino.cc/mkr-wan-1300)